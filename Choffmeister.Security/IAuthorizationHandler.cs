﻿using System;

namespace Choffmeister.Security
{
    /// <summary>
    /// This interface allows building a chain nested authorization
    /// handlers. See the chain of responsibility design pattern.
    /// </summary>
    public interface IAuthorizationHandler : IDisposable
    {
        /// <summary>
        /// Gets the name.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets or sets the successor handler.
        /// </summary>
        IAuthorizationHandler Successor { get; set; }

        /// <summary>
        /// Authorizes a verb on a domain.
        /// </summary>
        /// <param name="name">The verb.</param>
        /// <param name="domain">The domain.</param>
        /// <returns>True if the authorization is granted,
        /// false if not and null if this handler could not make
        /// a decision.</returns>
        bool? Authorize(string verb, object domain);
    }
}