﻿using System;

namespace Choffmeister.Security
{
    /// <summary>
    /// This interface allows building a chain nested authentication
    /// handlers. See the chain of responsibility design pattern.
    /// </summary>
    public interface IAuthenticationHandler : IDisposable
    {
        /// <summary>
        /// Gets the name.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets or sets the successor handler.
        /// </summary>
        IAuthenticationHandler Successor { get; set; }

        /// <summary>
        /// Authenticates a given pair of username and password.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <returns>True if the credentials could be authenticated,
        /// false if not and null if this handler could not make
        /// a decision.</returns>
        bool? Authenticate(string username, string password);
    }
}