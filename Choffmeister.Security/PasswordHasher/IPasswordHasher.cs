﻿namespace Choffmeister.Security.PasswordHasher
{
    public interface IPasswordHasher
    {
        string GenerateSalt();

        string HashPassword(string password, string salt);
    }
}