﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Choffmeister.Security.PasswordHasher
{
    public sealed class SHA512PasswordHasher : IPasswordHasher
    {
        private static UTF8Encoding _utf8 = new UTF8Encoding();
        private static SHA512Managed _sha512 = new SHA512Managed();
        private static RNGCryptoServiceProvider _rng = new RNGCryptoServiceProvider();

        private int _rounds;

        public SHA512PasswordHasher()
            : this(1024)
        {
        }

        public SHA512PasswordHasher(int rounds)
        {
            if (rounds < 1)
                throw new ArgumentOutOfRangeException("Hasher must do at least one round.");

            _rounds = rounds;
        }

        public string GenerateSalt()
        {
            byte[] buff = new byte[32];
            _rng.GetBytes(buff);

            return Convert.ToBase64String(_sha512.ComputeHash(buff));
        }

        public string HashPassword(string password, string salt)
        {
            var passwordBytes = _utf8.GetBytes(password ?? string.Empty);
            var saltBytes = _utf8.GetBytes(salt ?? string.Empty);

            for (int i = 0; i < _rounds; i++)
            {
                passwordBytes = _sha512.ComputeHash(passwordBytes.Concat(saltBytes).ToArray());
            }

            return Convert.ToBase64String(passwordBytes);
        }
    }
}