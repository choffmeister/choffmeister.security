﻿namespace Choffmeister.Security
{
    public sealed class DenyAuthorizationHandler : AuthorizationHandlerBase
    {
        public DenyAuthorizationHandler()
            : base("deny")
        {
        }

        public override bool? Authorize(string verb, object domain)
        {
            return false;
        }
    }
}