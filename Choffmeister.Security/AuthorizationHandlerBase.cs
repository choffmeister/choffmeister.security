﻿namespace Choffmeister.Security
{
    public abstract class AuthorizationHandlerBase : IAuthorizationHandler
    {
        private readonly string _name;

        public string Name
        {
            get { return _name; }
        }

        public IAuthorizationHandler Successor { get; set; }

        public AuthorizationHandlerBase(string name)
        {
            _name = name;
        }

        public abstract bool? Authorize(string verb, object domain);

        public void Dispose()
        {
            if (this.Successor != null)
            {
                this.Successor.Dispose();
            }

            this.OnDispose(true);
        }

        protected virtual void OnDispose(bool disposing)
        {
        }
    }
}