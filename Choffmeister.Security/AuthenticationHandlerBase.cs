﻿namespace Choffmeister.Security
{
    public abstract class AuthenticationHandlerBase : IAuthenticationHandler
    {
        private readonly string _name;

        public string Name
        {
            get { return _name; }
        }

        public IAuthenticationHandler Successor { get; set; }

        public AuthenticationHandlerBase(string name)
        {
            _name = name;
        }

        public abstract bool? Authenticate(string username, string password);

        public void Dispose()
        {
            if (this.Successor != null)
            {
                this.Successor.Dispose();
            }

            this.OnDispose(true);
        }

        protected virtual void OnDispose(bool disposing)
        {
        }
    }
}