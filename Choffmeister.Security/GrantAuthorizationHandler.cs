﻿namespace Choffmeister.Security
{
    public sealed class GrantAuthorizationHandler : AuthorizationHandlerBase
    {
        public GrantAuthorizationHandler()
            : base("grant")
        {
        }

        public override bool? Authorize(string verb, object domain)
        {
            return true;
        }
    }
}